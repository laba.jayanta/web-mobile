Ketentuan Projek
1. Peserta diharuskan menggunakan framework berbasis web (Laravel atau CI)
2. Aplikasi dikembangan menggunakan aplikasi database MySQL
3. Aplikasi yang dibuat adalah aplikasi terkait perekaman data matakuliah
4. Sebagai acuan ketentuan perekaman data matakuliah mengacu pada data program studi dan matakuliah
5. Programer di perbolehkan melakukan penambahan data table untuk menyimpan data matakuliah maupun program studi
6. Penentuan kode matakulaih didapat dari:
    GEOS119101 
    3 digit kode program studi (GEO untuk kode prefix Pendidikan Geografi)
    2 digit jenjang prodi (D3, S1, S2, S3)
    2 digit tahun kurikulum (19)
    1 digit semester kemuncula (1)
    2 digit nomor urut matakuiah setiap semester
7. Nomor urut matakuliah dimulai dari nomor 1 setiap semester yang berbeda

Bisnis Proses Sistem
1. User harus login terlebih dahulu untuk menggunakan sistem (User terdiri dari kategori operator dan mahasiswa sesuai prodi)
2. Operator dapat melakukan dapat melakukan operasi CRUD sesuai login prodi
3. Mahasiwa hanya dapat melakukan view untuk matakuliah yang terdaftar sesuai dengan program studi
4. Koneksi penyimpanan data maupun view menggunakan API